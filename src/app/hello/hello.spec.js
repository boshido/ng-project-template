describe( 'HelloCtrl', function() 
{
  describe( 'Positive Testing', function() 
  {
    var helloCtrl,$scope,orderModel;
    beforeEach(module('App.Hello'));
    beforeEach(inject(function($controller, $rootScope)
    {
      $scope = $rootScope.$new();
      helloCtrl = $controller('HelloCtrl',{$scope:$scope});
    })); 

    it('scope.text = "Hello World!"',inject(function()
    {
      expect($scope.text).toBe('Hello World!');
    }));
  });
});