describe( 'AppCtrl', function() 
{
  describe( 'Positive Testing', function() 
  {
    var appCtrl,$scope,orderModel;
    beforeEach(module('App'));
    beforeEach(inject(function($controller, $rootScope, $httpBackend)
    {
      $scope = $rootScope.$new();
      appCtrl = $controller('AppCtrl',{$scope:$scope});
    })); 

    it('initail scope',inject(function()
    {
      expect($scope).toNotBe(undefined);
    }));
  });
});