angular.module( 'App', [
	'ui.router',
  'App.Hello'
])
.config( [ '$stateProvider','$urlRouterProvider',function($stateProvider,$urlRouterProvider)  
{
  $stateProvider.state('hello', {
    url:'/',
    views: {
      'mainView' : {
        controller: 'HelloCtrl',
        templateUrl: 'app/hello/hello.tpl.html'
      }
    }
  });

  $urlRouterProvider.otherwise('/');
}])
.controller('AppCtrl',['$rootScope','$scope',function ($rootScope, $scope)
{

}]);

