var appConfig          = require('./app.conf');
module.exports = function(config) {
  config.set({
    frameworks: ['jasmine'],

    files: [
      './vendor/angular/angular.js',
      './vendor/angular-mocks/angular-mocks.js',
      './vendor/angular-ui-router/release/angular-ui-router.js',
      './src/**/*.js',
      './src/**/*.spec.js',
      './src/**/*.tpl.html',
      'https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAdhWBcIvHUlUsZs2Npkl5Rpi4RNxQ_If0'
    ],

    preprocessors: {
      './src/**/*.tpl.html': ['ng-html2js']
    },

    browsers:['PhantomJS'],

    plugins : [
      'karma-junit-reporter',
      'karma-chrome-launcher',
      'karma-firefox-launcher',
      'karma-opera-launcher',
      'karma-ie-launcher',
      'karma-jasmine',
      'karma-ng-html2js-preprocessor',
      'karma-phantomjs-launcher'
    ],

    ngHtml2JsPreprocessor: {
      // strip this from the file path
      
      // prepend this to the

      stripPrefix: 'src/',
      // prependPrefix: '/',

      // // or define a custom transform function
      // cacheIdFromPath: function(filepath) {
      //   console.log(filepath)
      //   // return filepath;
      // },

      // setting this option will create only a single module that contains templates
      // from all the files, so you can load them all with module('foo')
      moduleName: appConfig.moduleName
    }
  });
};