module.exports = {
  /**
   * The `buildDirectory` folder is where projects are compiled during
   * development and the `compileDirectory` folder is where app resides once it's
   * completely built.
   */
  moduleName        : 'App',
  buildDirectory    : 'build/',
  compileDirectory  : 'dist/',

  /**
   * This is a collection of file patterns that refer to our app code (the
   * stuff in `src/`). These file paths are used in the configuration of
   * build and compile tasks.
   */

  appFiles : {
    script          :   [ 'src/**/*.js', '!src/**/*.spec.js', '!src/asset/**/*.js' , '!src/css/**/*.js'],
    test            :   [ 'src/**/*.spec.js' ],
    template        :   [ 'src/**/*.tpl.html' ],
    html            :   [ 'src/index.html' ],
    asset           :   [ 'src/asset/**/*.*' ],
    css             :   [ 'src/**/*.css', 'src/css/main.less' ]
  },

  buildFiles:{
    dependencies    :   'lib',
    script          :   'script',
    asset           :   'asset',
    css             :   'css'
      
  },

  compileFiles:{
    dependencies    :   'lib',
    script          :   'asset',
    asset           :   'asset',
    css             :   'asset'
  },

  dependenciesOrder : [
    "**/jquery.js",
    "**/angular.js",
    "**/bootstrap.js"
  ]
};
