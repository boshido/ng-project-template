var config      = require('./app.conf');
var gulp        = require('gulp');
var requireDir  = require('require-dir');
var dir         = requireDir('./tasks');
var runSequence = require('run-sequence');
var connect     = require('gulp-connect');
var karma       = require('gulp-karma')

var watchFiles = {
  script : [config.appFiles.script, config.appFiles.test],
  template : config.appFiles.template,
  html : config.appFiles.html,
  asset : config.appFiles.asset,
  css : ['src/**/*.less', 'src/**/*.css']
}

gulp.task('test',function()
{
  return gulp.src('NULL')
          .pipe(
              karma({
              configFile:'karma.conf.js',
              action:'run'
              })
          );
});

gulp.task('reload',function()
{
  return gulp.src(config.buildDirectory+'**')
      .pipe(connect.reload());
});

gulp.task('watch:style',function()
{
  connect.server({
    root: config.buildDirectory,
    port: 8080,
    livereload: true
  });
  
  runSequence('build:style','reload','watch:script-no-test','watch:template','watch:html','watch:asset','watch:css');    
});

gulp.task('watch',function()
{
  connect.server({
    root: config.buildDirectory,
    port: 8080,
    livereload: true
  });

  runSequence('build','reload','watch:script','watch:template','watch:html','watch:asset','watch:css');
});

gulp.task('watch:script-no-test', function()
{
  gulp.watch(watchFiles.script, function()
  {
    runSequence('build:move-script','build:add-path','reload');
  });
});

gulp.task('watch:script', function()
{
  gulp.watch(watchFiles.script, function()
  {
    runSequence('test','build:move-script','build:add-path','reload');
  });
});

gulp.task('watch:template', function()
{
  gulp.watch(watchFiles.template, function()
  {
    runSequence('build:move-template:html','build:add-path','reload');
  });
});

gulp.task('watch:html', function()
{
  gulp.watch(watchFiles.html, function()
  {
    runSequence('build:add-path','reload');
  });
});

gulp.task('watch:asset', function()
{
  gulp.watch(watchFiles.asset, function()
  {
    runSequence('build:move-asset','build:add-path','reload');
  });
});

gulp.task('watch:css', function()
{
  gulp.watch(watchFiles.css, function()
  {
    runSequence('build:move-css','build:add-path','reload');
  });
});
 
gulp.task('default',['watch','compile']);