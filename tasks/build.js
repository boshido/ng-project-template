var config          = require('../app.conf'),
    gulp            = require('gulp'),
    rimraf          = require('gulp-rimraf'),
    concat          = require('gulp-concat'),
    notify          = require('gulp-notify'),
    mainBowerFiles  = require('main-bower-files'),
    inject          = require('gulp-inject'),
    _if             = require('gulp-if'),
    ngHtml2Js       = require("gulp-ng-html2js"),
    order           = require("gulp-order"), 
    less            = require('gulp-less'),
    jshint          = require('gulp-jshint'),
    runSequence     = require('run-sequence'),
    isWindows = /^win/.test(require('os').platform());

gulp.task('build:clean',function()
{
  return gulp.src(config.buildDirectory,{read: false})
          .pipe(rimraf());
});

gulp.task('build:move-template:js',function()
{
  return gulp.src(config.appFiles.template)
          .pipe(ngHtml2Js({
              moduleName: config.moduleName
          }))
          .pipe(concat("template.js"))
          .pipe(gulp.dest(config.buildDirectory+config.buildFiles.script));
});

gulp.task('build:move-template:html',function()
{
  return gulp.src(config.appFiles.template)
          .pipe(gulp.dest(config.buildDirectory));
});

gulp.task('build:move-dependencies',function()
{
  return gulp.src(mainBowerFiles())
          .pipe(gulp.dest(config.buildDirectory+config.buildFiles.dependencies));  
})

gulp.task('build:move-script',function()
{
  return gulp.src(config.appFiles.script)
          .pipe(jshint())
          .pipe(jshint.reporter('jshint-stylish'))
          .pipe(gulp.dest(config.buildDirectory+config.buildFiles.script))
});

gulp.task('build:move-asset',function()
{
  return gulp.src(config.appFiles.asset)
          .pipe(gulp.dest(config.buildDirectory+config.buildFiles.asset))
});

gulp.task('build:move-css',function()
{
  return gulp.src(config.appFiles.css)
          .pipe(less())
          .pipe(concat('main.css'))
          .pipe(gulp.dest(config.buildDirectory+config.buildFiles.css))
});

gulp.task('build:add-path',function()
{
  var injectFiles = [
      config.buildDirectory+config.buildFiles.dependencies+'/**/*.js',
      config.buildDirectory+config.buildFiles.dependencies+'/**/*.css',
      config.buildDirectory+config.buildFiles.script+'/**/*.js',
      config.buildDirectory+config.buildFiles.css+'/**/*.css',
  ];
  
  return gulp.src(config.appFiles.html)
          .pipe(
              inject( gulp.src(injectFiles, {read: false}).pipe(order(config.dependenciesOrder)),{addRootSlash:false,ignorePath: '/build/'})
          )
          .pipe(gulp.dest(config.buildDirectory))
          .pipe(_if(!isWindows,notify({message:'Build Success'})));
});

gulp.task('build:style',function(callback)
{
  runSequence('build:clean',['build:move-dependencies','build:move-script','build:move-asset','build:move-css','build:move-template:html'],'build:add-path',
            callback);
});

gulp.task('build:js-template',function(callback)
{
  runSequence('test','build:clean',['build:move-dependencies','build:move-script','build:move-asset','build:move-css','build:move-template:js'],'build:add-path',
            callback);
});

gulp.task('build',function(callback)
{
  runSequence('test','build:clean',['build:move-dependencies','build:move-script','build:move-asset','build:move-css','build:move-template:html'],'build:add-path',
            callback);
});