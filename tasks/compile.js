var config          = require('../app.conf'),
    gulp            = require('gulp'),
    rimraf          = require('gulp-rimraf'),
    concat          = require('gulp-concat'),
    notify          = require('gulp-notify'),
    mainBowerFiles  = require('main-bower-files'),
    inject          = require('gulp-inject'),
    _if             = require('gulp-if'),
    ngHtml2Js       = require('gulp-ng-html2js'),
    uglify          = require('gulp-uglify'),
    minifyCss       = require('gulp-minify-css'),
    minifyHtml      = require('gulp-minify-html'),
    order           = require('gulp-order'),
    less            = require('gulp-less'),
    jshint          = require('gulp-jshint'),
    runSequence     = require('run-sequence'),
    eventStream     = require('event-stream'),
    isWindows = /^win/.test(require('os').platform());

gulp.task('compile:clean',function()
{
  return gulp.src(config.compileDirectory,{read: false})
          .pipe(rimraf());
});

gulp.task('compile:move-script',function()
{
  var js = '**/' + config.appFiles.script[0];

  return gulp.src(mainBowerFiles().concat(config.appFiles.script))
          .pipe(
            _if(js,jshint())
          )
          .pipe(
            _if(js,jshint.reporter('jshint-stylish'))
          )
          .pipe(
            _if(js,jshint())
          )
          .pipe(order(config.dependenciesOrder))
          .pipe( 
            _if('*.js',concat('ng.js'))
          )
          .pipe( 
            _if('*.js',uglify())
          )
          .pipe(
            _if(/(.js)/,
              gulp.dest(config.compileDirectory+config.compileFiles.script),
              gulp.dest(config.compileDirectory+config.compileFiles.dependencies)
            )
          );
});

gulp.task('compile:move-script:js-template',function()
{
  var template = '**/' + config.appFiles.template[0],
      js = '**/' + config.appFiles.script[0];

  return gulp.src(mainBowerFiles().concat(config.appFiles.template,config.appFiles.script))
          .pipe(
            _if('**/*.tpl.html',minifyHtml({
              empty: true,
              spare: true,
              quotes: true
            }))
          )
          .pipe(
            _if(template,ngHtml2Js({
              moduleName: config.moduleName
            }))
          )
          .pipe(
            _if(js,jshint())
          )
          .pipe(
            _if(js,jshint.reporter('jshint-stylish'))
          )
          .pipe(
            _if(js,jshint())
          )
          .pipe(order(config.dependenciesOrder))
          .pipe( 
            _if('*.js',concat('ng.js'))
          )
          .pipe( 
            _if('*.js',uglify())
          )
          .pipe(
            _if(/(.js)/,
              gulp.dest(config.compileDirectory+config.compileFiles.script),
              gulp.dest(config.compileDirectory+config.compileFiles.dependencies)
            )
          );
});

gulp.task('compile:move-asset',function()
{
  return gulp.src(config.appFiles.asset)
          .pipe(gulp.dest(config.compileDirectory+config.compileFiles.asset))
});

gulp.task('compile:move-css',function()
{
  return gulp.src(config.appFiles.css)
          .pipe(less())
          .pipe(minifyCss())
          .pipe(gulp.dest(config.compileDirectory+config.compileFiles.css))
});

gulp.task('compile:move-template',function()
{
  return gulp.src(config.appFiles.template)
          .pipe(gulp.dest(config.compileDirectory))
});

gulp.task('compile:add-path',function()
{
  var injectFiles = [
      config.compileDirectory+config.compileFiles.dependencies+'/**/*.js',
      config.compileDirectory+config.compileFiles.dependencies+'/**/*.css',
      config.compileDirectory+config.compileFiles.script+'/**/*.js',
      config.compileDirectory+config.compileFiles.css+'/**/*.css',
  ];
  
  return gulp.src(config.appFiles.html)
          .pipe(
            inject( gulp.src(injectFiles, {read: false}).pipe(order(config.dependenciesOrder)),{addRootSlash:false,ignorePath: config.compileDirectory})
          )
          .pipe(gulp.dest(config.compileDirectory))
          .pipe(_if(!isWindows,notify({message:'compile Success'})));
});

gulp.task('compile:style',function(callback)
{
  runSequence('compile:clean','compile:move-script','compile:move-asset','compile:move-css','compile:move-template','compile:add-path',
            callback);
});

gulp.task('compile:js-template',function(callback)
{
  runSequence('test','compile:clean','compile:move-script:js-template','compile:move-asset','compile:move-css','compile:add-path',
            callback);
});

gulp.task('compile',function(callback)
{
  runSequence('test','compile:clean','compile:move-script','compile:move-asset','compile:move-css','compile:move-template','compile:add-path',
            callback);
});